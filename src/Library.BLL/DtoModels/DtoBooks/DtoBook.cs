﻿using System.ComponentModel.DataAnnotations;

namespace Library.BLL.DtoModels.DtoBooks
{
    public sealed class DtoBook
    {
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength: 40)]
        public string BookName { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
