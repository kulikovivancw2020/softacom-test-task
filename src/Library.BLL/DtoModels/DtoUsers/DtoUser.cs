﻿namespace Library.BLL.DtoModels.DtoUsers
{
    public  class DtoUser
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

    }
}
