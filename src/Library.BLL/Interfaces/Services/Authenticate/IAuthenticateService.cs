﻿using Library.BLL.AuthenticateModels;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces.Services.Authenticate
{
    public interface IAuthenticateService
    {
       Task<AuthenticateResponse> Authenticate(AuthenticateRequest request);
    }
}
