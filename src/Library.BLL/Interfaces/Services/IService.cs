﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces.Services
{
    public interface IService<DtoModel> where DtoModel : class
    {
        Task AddAcyns(DtoModel model);

        List<DtoModel> GetAll();

        Task DeleteAsync(int? id);

        Task UpdateAsync(DtoModel model);
    }
}
