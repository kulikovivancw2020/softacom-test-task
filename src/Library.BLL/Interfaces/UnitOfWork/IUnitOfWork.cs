﻿using Library.DAL.Entities.Books;
using Library.DAL.Interfaces;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Book> BookRepository { get; }

        Task SaveAsync();
    }
}
