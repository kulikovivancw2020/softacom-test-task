﻿using Library.BLL.AuthenticateModels;
using Library.BLL.Helpers;
using Library.BLL.Interfaces.Services.Authenticate;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Library.BLL.Services.Authenticate
{
    public sealed class AuthenticateService : IAuthenticateService
    {
        private readonly IConfiguration _configuration;
        private readonly UserManager<IdentityUser> _userManager;

        public AuthenticateService(IConfiguration configuration, UserManager<IdentityUser> userManager)
        {
            _configuration = configuration;
            _userManager = userManager;
        }

        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
            {
                return null;
            }

            var result = await _userManager.CheckPasswordAsync(user, request.Password);

            if (!result)
            {
                return null;
            }

            var token = AuthenticateHelper.GenerateJwtToken(_configuration, user);
            var response = new AuthenticateResponse(user,token);

            return response;
        }
    }
}
