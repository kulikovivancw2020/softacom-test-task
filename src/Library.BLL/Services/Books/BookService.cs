﻿using AutoMapper;
using Library.BLL.DtoModels.DtoBooks;
using Library.BLL.Interfaces.Services;
using Library.BLL.Interfaces.UnitOfWork;
using Library.DAL.Entities.Books;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.BLL.Services.Books
{
    public sealed class BookService : IService<DtoBook>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BookService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAcyns(DtoBook dtoBook)
        {
            var book = _mapper.Map<Book>(dtoBook);
            await _unitOfWork.BookRepository.CreateAsync(book);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(int? id)
        {
            var book = _unitOfWork.BookRepository.Get().FirstOrDefault(b => b.Id == id);

            if (book == null)
            {
                _unitOfWork.BookRepository.Delete(book);
            }

            await _unitOfWork.SaveAsync();
        }

        public List<DtoBook> GetAll()
        {
            var booksList = _unitOfWork.BookRepository.Get().ToList();
            var dtoBooksList = _mapper.Map<List<DtoBook>>(booksList);
            return dtoBooksList;
        }

        public async Task UpdateAsync(DtoBook dtoBook)
        {
            var book = _mapper.Map<Book>(dtoBook);
            _unitOfWork.BookRepository.Update(book);
            await _unitOfWork.SaveAsync();
        }
    }
}
