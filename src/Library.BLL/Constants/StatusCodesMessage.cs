﻿namespace Library.BLL.Constants
{
    public sealed class StatusCodesMessage
    {
        public const string Code400MessageDataIsNull = "Error with data";

        public const string Code400MessageCreateIsFailed = "Object cannot be create";

        public const string Code400MessageNotFound = "Object cannot be found";
    }
}
