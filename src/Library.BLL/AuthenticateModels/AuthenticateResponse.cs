﻿using Microsoft.AspNetCore.Identity;

namespace Library.BLL.AuthenticateModels
{
    public class AuthenticateResponse
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }

        public AuthenticateResponse(IdentityUser user, string token)
        {
            Username = user.UserName;
            Email = user.Email;
            Token = token;
        }
    }
}
