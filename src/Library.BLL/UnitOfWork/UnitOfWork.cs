﻿using Library.BLL.Interfaces.UnitOfWork;
using Library.DAL.ApplicationContext;
using Library.DAL.Entities.Books;
using Library.DAL.Interfaces;
using Library.DAL.Repository;
using System.Threading.Tasks;

namespace Library.BLL.UnitOfWork
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private  IGenericRepository<Book> _genericBookRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public IGenericRepository<Book> BookRepository
        {
            get
            {
                if (_genericBookRepository == null) _genericBookRepository = new GenericRepository<Book>(_context);
                return _genericBookRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
