﻿using AutoMapper;
using Library.BLL.DtoModels.DtoUsers;
using Microsoft.AspNetCore.Identity;

namespace Library.BLL.MappingProfiles.UserMapping
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<DtoUser, IdentityUser>();

            CreateMap<IdentityUser, DtoUser>();
        }
    }
}
