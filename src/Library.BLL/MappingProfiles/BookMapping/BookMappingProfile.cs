﻿using AutoMapper;
using Library.BLL.DtoModels.DtoBooks;
using Library.DAL.Entities.Books;

namespace Library.BLL.MappingProfiles.BookMapping
{
    public class BookMappingProfile : Profile
    {
        public BookMappingProfile()
        {
            CreateMap<DtoBook, Book>();

            CreateMap<Book, DtoBook>();
        }
    }
}
