﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
#pragma warning disable CA1050 
public sealed class AuthorizeAttribute : Attribute, IAuthorizationFilter
#pragma warning restore CA1050 
{
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var user = (IdentityUser)context.HttpContext.Items["User"];
        if (user == null)
        {
            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
        }
    }
}
