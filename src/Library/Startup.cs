using AutoMapper;
using Library.BLL.DtoModels.DtoBooks;
using Library.BLL.Interfaces.Services;
using Library.BLL.Interfaces.Services.Authenticate;
using Library.BLL.Interfaces.UnitOfWork;
using Library.BLL.MappingProfiles.BookMapping;
using Library.BLL.MappingProfiles.UserMapping;
using Library.BLL.Services.Authenticate;
using Library.BLL.Services.Books;
using Library.BLL.UnitOfWork;
using Library.DAL.ApplicationContext;
using Library.Ui.Middlewares.Jwt;
using Library.UI.Middlewares.ExceptionCatcher;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Library
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<ApplicationDbContext>(options => options
                .UseSqlServer(Configuration
                .GetConnectionString("DataConnection")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IUnitOfWork,UnitOfWork>();
            services.AddTransient<IService<DtoBook>,BookService>();
            services.AddTransient<IAuthenticateService, AuthenticateService>();

            services.AddCors();


            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new BookMappingProfile());
                mc.AddProfile(new UserMappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Library", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Library v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseMiddleware<JwtMiddleware>();
            app.UseMiddleware<ExceptionCatcherMiddleware>();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
