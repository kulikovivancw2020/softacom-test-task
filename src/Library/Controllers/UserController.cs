﻿using AutoMapper;
using Library.BLL.AuthenticateModels;
using Library.BLL.Constants;
using Library.BLL.DtoModels.DtoUsers;
using Library.BLL.Interfaces.Services.Authenticate;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Library.UI.Controllers
{
    [Route("api")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;

        private readonly SignInManager<IdentityUser> _signInManager;

        private readonly IAuthenticateService _authenticateService;

        private readonly IMapper _mapper;


        public UserController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IMapper mapper, IAuthenticateService authenticateService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _authenticateService = authenticateService;
        }

        [HttpPost]
        public async Task<IActionResult> Registration(DtoUser dtoUser)
        {
            if (dtoUser == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, StatusCodesMessage.Code400MessageDataIsNull);
            }

            var user = _mapper.Map<IdentityUser>(dtoUser);

            var result = await _userManager.CreateAsync(user, dtoUser.Password);

            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status400BadRequest, StatusCodesMessage.Code400MessageCreateIsFailed);
            }

            return Ok();
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> LogIn(AuthenticateRequest authenticateRequest)
        {
            if (authenticateRequest == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, StatusCodesMessage.Code400MessageDataIsNull);
            }

            var response = await _authenticateService.Authenticate(authenticateRequest);

            return Ok(response);
        }
    }
}
