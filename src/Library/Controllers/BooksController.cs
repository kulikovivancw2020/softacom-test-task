﻿using Library.BLL.Constants;
using Library.BLL.DtoModels.DtoBooks;
using Library.BLL.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.UI.Controllers
{
    [Route("api/book")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IService<DtoBook> _bookService;
        public BooksController(IService<DtoBook> bookService)
        {
            _bookService = bookService;
        }

        #region CRUD
        [HttpPost]
        [Authorize]

        public async Task<IActionResult> CreateBook(DtoBook dtoBook)
        {
            if (dtoBook == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest , StatusCodesMessage.Code400MessageDataIsNull);
            }

            await _bookService.AddAcyns(dtoBook);    

            return Ok();
        }

        [HttpGet]
        [Authorize]
        public ActionResult<List<DtoBook>> GetBooks()
        {
            var books = _bookService.GetAll();

            return books;
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateBook(DtoBook dtoBook)
        {
            if (dtoBook == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, StatusCodesMessage.Code400MessageDataIsNull);
            }

            await _bookService.UpdateAsync(dtoBook);

            return Ok();
        }

        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> DeleteBook(int? id)
        {
            if (id == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, StatusCodesMessage.Code400MessageDataIsNull);
            }

            await _bookService.DeleteAsync(id);

            return Ok();
        }
        #endregion CRUD
    }
}
