﻿namespace Library.DAL.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
