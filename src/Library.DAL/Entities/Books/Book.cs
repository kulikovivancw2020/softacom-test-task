﻿using System.ComponentModel.DataAnnotations;

namespace Library.DAL.Entities.Books
{
    public class Book : BaseEntity
    {
        [Required]
        [StringLength(maximumLength: 40)]
        public string BookName { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
