﻿using Library.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Library.DAL.ApplicationContext;

namespace Library.DAL.Repository
{
    public sealed class GenericRepository<TEntity> :IGenericRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(ApplicationDbContext context)
        {
            _dbSet = context.Set<TEntity>();
        }

        public async Task CreateAsync(TEntity obj)
        {
            await _dbSet.AddAsync(obj);
        }

        public void Delete(TEntity obj)
        {
            _dbSet.Remove(obj);
        }

        public IQueryable<TEntity> Get()
        {
            var objList = _dbSet.AsQueryable();
            return objList;
        }

        public void Update(TEntity obj)
        {
            _dbSet.Update(obj);
        }
    }
}
