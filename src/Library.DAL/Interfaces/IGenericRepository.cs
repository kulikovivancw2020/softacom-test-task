﻿using System.Linq;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    public interface IGenericRepository<TEntity>  where TEntity : class
    {
        Task CreateAsync(TEntity obj);

        IQueryable<TEntity> Get();

        void Update(TEntity obj);

        void Delete(TEntity obj);
    }
}
